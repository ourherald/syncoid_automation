#!/usr/bin/env python3

import subprocess
from subprocess import check_output
import os
import yaml
import logging

# Load variables from YAML file
__location__ = os.path.realpath(
	os.path.join(os.getcwd(), os.path.dirname(__file__)))

with open(os.path.join(__location__, 'config.yaml'), 'r') as file:
	config_data = yaml.safe_load(file)

#for key, value in config_data.get('env_variables', {}).items():
#	os.environ[key] = value

source_data = config_data.get('source_data', {})


# Global vars
syncoid_cmd = config_data['env_variables']['SYNCOID_CMD']
syncoid_opts = config_data['env_variables'].get('syncoid_opts', [])
dest_zpool = config_data['env_variables']['DEST_ZPOOL']
dest_dataset = config_data['env_variables']['DEST_DATASET']

if 'SSH_USER' in config_data['env_variables']:
	ssh_user = config_data['env_variables']['SSH_USER']
else:
	## Use environment variables for SSH credentials and private key path
	ssh_user = os.getenv('SSH_USER')

ssh_private_key = os.getenv('SSH_PRIVATE_KEY')

#chk_process = 'tmux'
chk_process = 'syncoid'

# Set up logging
logfile = config_data['env_variables']['LOGFILE']
#logfile = '/var/log/syncoid.log'

logging.basicConfig(filename=logfile,
                    filemode='a',
                    format='%(asctime)s %(levelname)s %(message)s',
                    #datefmt='%H:%M:%S',
                    level=logging.DEBUG)

# Check for running syncoid jobs
def is_syncoid_running():
	try:
		# Run 'pidof syncoid' command and capture the output
		pid_output = subprocess.check_output(['pidof', chk_process], text=True)

		# Convert the output to a list of PIDs
		pids = pid_output.strip().split()

		# Check if any PID is present
		return bool(pids)
	except subprocess.CalledProcessError:
		# If 'pidof' command returns a non-zero exit code, the process is not running
		return False

# The syncing part
def main():
	for site, server in source_data.items():
		for server, zpool in server.items():
			logging.info("Running sync for " + str(site) + "/" + str(server))
			for zpool, datasets in zpool.items():
				for dataset in datasets:
					logging.info("|--> Processing " + str(zpool) + "/" + str(dataset))
					source = f"{ssh_user}@{server}:{zpool}/{dataset}"
					destination = f"{dest_zpool}/{dest_dataset}/{site}/{server}/{dataset}"
					#command = subprocess.run(['echo', 'syncoid', source, destination] + syncoid_opts, capture_output=True, text=True)	
					command = subprocess.run([syncoid_cmd, source, destination] + syncoid_opts, capture_output=True, text=True)	
					if command.returncode == 0:
						logging.info(command.stdout)
					else:
						logging.error(command.stderr)

		
# Do the script
with open(logfile, "a") as log:
	print("***", file=log)

if is_syncoid_running():
	logging.warning("Sync process is already running, try again later")
	exit(1)
else:
	logging.info("Starting sync process")

if __name__ == "__main__":
	main()

with open(logfile, "a") as log:
    print("===", file=log)
