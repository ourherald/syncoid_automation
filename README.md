# Syncoid Automation

This is a python script and associated YAML file to use [Jim Salter's `syncoid`](https://github.com/jimsalterjrs/sanoid) as the method for pulling backups from multiple servers with ZFS datasets.

Use the `config.yaml` to specify relevant environment variables and what will become a dictionary of servers, zpools, and datasets.
